from django.urls import path
from pantry.views import home_page, pantry_list, ingredient_suggestion, pantry_suggestion, move_item, pantry_delete_confirmation, pantry_delete_redirect

urlpatterns = [
    path("my_pantry", pantry_list, name="pantry_list"),
    path("home/", home_page, name='home_page'),
    path("ingredient_suggestion/<int:id>/", ingredient_suggestion, name="ingredient_suggestion"),
    path("pantry_suggestion/", pantry_suggestion, name="pantry_suggestion"),
    path("move_item/<int:id>/", move_item, name="move_item"),
    path("pantry_delete_confirmation/<int:id>/", pantry_delete_confirmation, name="pantry_delete_confirmation"),
    path("pantry_delete_redirect/", pantry_delete_redirect, name="pantry_delete_redirect"),
]
