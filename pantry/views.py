from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from pantry.models import PantryItem
from pantry.forms import PantryItemForm, InPantryForm
from recipes.models import Recipe
# Create your views here.

def home_page(request):
    return render(request, "pantry/home.html")

def pantry_list(request):
    pantry_item = PantryItem.objects.all()
    if request.method == "POST":
        add_form = PantryItemForm(request.POST)
        if add_form.is_valid():
            add_form.save()
            return redirect("pantry_list")
        else:
            add_form.add_error("name", "That's already in here")
    else:
        add_form = PantryItemForm()

    context = {
        "pantry_item": pantry_item,
        "add_form": add_form,
    }
    return render(request, "pantry/my_pantry.html", context)

def ingredient_suggestion(request, id):
    match_recipes = []
    sub_recipes = []
    recipes = Recipe.objects.all()
    ingredient = PantryItem.objects.get(id=id)
    for recipe in recipes:
        if ingredient == recipe.main_ingredient:
            match_recipes.append(recipe)
    for recipe in recipes:
        if ingredient in recipe.ingredients.all() and recipe not in match_recipes:
            sub_recipes.append(recipe)
    context = {
        "recipes": recipes,
        "ingredient": ingredient,
        "match_recipes": match_recipes,
        "sub_recipes": sub_recipes,
    }
    return render(request, "pantry/ingredient_suggested.html", context)

def pantry_suggestion(request):
    pantry = PantryItem.objects.all()
    pantry_items = []
    for item in pantry:
        if item.in_pantry == True:
            pantry_items.append(item.name)
    recipes = Recipe.objects.all().order_by('name')
    ings = []
    matches = []
    for recipe in recipes:
        for ing in recipe.ingredients.all():
            ings.append(str(ing))
        if all(item in pantry_items for item in ings):
            matches.append(recipe)
        ings = []

    context = {
        "matches": matches,
        "pantry_items": pantry_items,
    }
    return render(request, "pantry/pantry_suggestion.html", context)

def move_item(request, id):
    item = get_object_or_404(PantryItem, id=id)
    name = item.name
    if request.method == "POST":
        form = InPantryForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("pantry_list")
    else:
        form = InPantryForm(instance=item)
    context = {
        "form": form,
        "name": name,
        "item": item,
    }
    return render(request, "pantry/move_item.html", context)

def pantry_delete_confirmation(request, id):
    item = PantryItem.objects.get(id=id)
    if request.method == "POST":
        item.delete()
        return redirect("pantry_list")
    context = {
        "item": item,
    }
    return render(request, "pantry/delete_confirmation.html")

def pantry_delete_redirect(request):
    return redirect('pantry_list')
