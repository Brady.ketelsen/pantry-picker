from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class PantryItem(models.Model):
    name = models.CharField(max_length=150, unique=True)
    in_pantry = models.BooleanField(default=True)

    def __str__(self):
        return self.name
    class Meta:
        ordering = ["name"]
