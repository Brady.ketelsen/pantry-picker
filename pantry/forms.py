from django import forms
from pantry.models import PantryItem

class PantryItemForm(forms.ModelForm):
    class Meta:
        model = PantryItem
        fields = [
            "name",
        ]

class InPantryForm(forms.ModelForm):
    class Meta:
        model = PantryItem
        fields = [
            "in_pantry"
        ]
