from django.contrib import admin
from pantry.models import PantryItem

# Register your models here.
@admin.register(PantryItem)
class PantryItemAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "in_pantry"
    )
