from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from accounts.forms import SignUpForm, LoginForm
from django.contrib.auth.models import User

# Create your views here.

def signup_user(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                    first_name=first_name,
                    last_name=last_name,
                )
                login (request, user)
                return redirect('home_page')
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, 'accounts/signup.html', context)

def login_user(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home_page")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)

def logout_user(request):
    logout(request)
    return redirect("home_page")
