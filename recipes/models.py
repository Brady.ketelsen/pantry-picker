from django.db import models
from pantry.models import PantryItem
from django.contrib.auth.models import User

# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    ingredients = models.ManyToManyField(PantryItem)
    main_ingredient = models.ForeignKey(PantryItem, related_name="main", on_delete=models.PROTECT)
