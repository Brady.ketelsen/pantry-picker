from django.shortcuts import render, redirect, get_object_or_404
from recipes.models import Recipe
from recipes.forms import RecipeForm
from pantry.forms import PantryItemForm
# Create your views here.

def recipe_list(request):
    recipes = Recipe.objects.all().order_by('name')
    context = {
        "recipes": recipes,
    }
    return render(request, "recipes/recipe_list.html", context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()

    if request.method == "POST":
        add_form = PantryItemForm(request.POST)
        if add_form.is_valid():
            add_form.save()
            return redirect("create_recipe")
        else:
            add_form.add_error("name", "That's already in here")
    else:
        add_form = PantryItemForm()

    context = {
        "form": form,
        "add_form": add_form,
    }
    return render(request, "recipes/create_recipe.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("recipe_detail", id=id)
    else:
        form = RecipeForm(instance=recipe)

    if request.method == "POST":
        add_form = PantryItemForm(request.POST)
        if add_form.is_valid():
            add_form.save()
            return redirect("edit_recipe", id=id)
        else:
            add_form.add_error("name", "That's already in here")
    else:
        add_form = PantryItemForm()

    context = {
        "recipe": recipe,
        "form": form,
        "add_form": add_form,
    }
    return render(request, 'recipes/edit.html', context)

def delete_confirmation(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        recipe.delete()
        return redirect("recipe_list")
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/delete_confirmation.html")

def recipe_detail(request, id):
    recipe = Recipe.objects.get(id=id)
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/detail.html", context)

def delete_redirect(request):
    return redirect('recipe_list')
