# Generated by Django 4.2 on 2023-04-26 16:55

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("recipes", "0001_initial"),
    ]

    operations = [
        migrations.DeleteModel(
            name="Recipe",
        ),
    ]
