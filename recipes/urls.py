from django.urls import path
from recipes.views import recipe_list, create_recipe, recipe_detail, delete_redirect, delete_confirmation, edit_recipe

urlpatterns = [
    path("", recipe_list, name="recipe_list"),
    path("create/", create_recipe, name="create_recipe"),
    path("edit_recipe/<int:id>/", edit_recipe, name="edit_recipe"),
    path("<int:id>/", recipe_detail, name="recipe_detail"),
    path("delete_redirect/", delete_redirect, name="delete_redirect"),
    path("delete_confirmation/<int:id>/", delete_confirmation, name="delete_confirmation"),
]
